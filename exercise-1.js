//Generators are functions that can be paused midway and continued from where paused!
//They produce a sequence of results instead of a single value
//These functions simplify an iterator
function* genFunction() {
  yield 1; //the yield keyword pauses the generator function
  yield 2;
}

const x = genFunction();

console.log(x.next()); //generator functions return an object on which we can call next()
console.log(x.next()); //the value of the obj returned by next() is {value: any , done: true|false}
console.log(x.next()); //when the done becomes true(no more yields), generator function stops and the value returned is undefined.
