//Make the following object iterable
let todoList = {
  todoItems: [],
  addItem(description) {
    this.todoItems.push({ description, done: false });
    return this;
  },
  crossOutItem(index) {
    if (index < this.todoItems.length) {
      this.todoItems[index].done = true;
    }
    return this;
  },
};

todoList.addItem('task 1').addItem('task 2').crossOutItem(0);

function* todoListGenerator() {
  yield todoList.todoItems;
}

let iterableTodoList = todoListGenerator();

for (let item of iterableTodoList) {
  console.log(item);
}
