//Print Fibonacci series upto 10 using Generator functions
//0,1,1,2,3,5,8,13
//i=0, j=1, next = i+j, i=j, j=next

function* fibGenerator() {
  var i = 0;
  var j = 1;
  yield i;
  yield j;
  var result = 0;

  while (result < 10) {
    result = i + j;
    yield result;
    i = j;
    j = result;
  }
}

console.log([...fibGenerator()]);
