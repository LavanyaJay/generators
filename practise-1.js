//Create a countdown iterator that counts from 9 to 1. Use generator functions!

function* countDown() {
  let i = 9;
  while (i > 0) {
    yield i;
    i--;
  }
}

/* for (const i of countDown()) {
  console.log(i);
} */
console.log([...countDown()]);
