//Generators to handle infinite data streams

function* naturalNum() {
  let num = 1;
  while (true) {
    yield num;
    num++;
  }
}

const numbers = naturalNum();

console.log(numbers.next().value);
console.log(numbers.next().value);

//As the above while loop continues to be true after every yield, the generator function does not end and continues to be true
