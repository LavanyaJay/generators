//Generators as observers

function* genObserver() {
  console.log('I am a Observer gen fn');
  console.log(`1: ${yield}`);
  console.log(`2: ${yield}`);
  return 'result';
}

const x = genObserver();

console.log(x.next());
console.log(x.next('a')); //Send value to first yield
console.log(x.next('b')); //Send value to second yield
// Sending value in next() is asymmetric, it send value to suspended yield, but return the operand of the following yield
