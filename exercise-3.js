//Implementing Async functionality using generators

//Old method using Promise chain
var axios = require('axios');
const fetchUsers = () => {
  axios
    .get('https://rem-rest-api.herokuapp.com/api/users')
    .then((users) => users.data)
    .then((result) => {
      console.log(result);
      return result;
    })

    .catch((err) => console.log(err));
};
//const x = fetchUsers();

//New method using generators

function* genToFetchUsers() {
  try {
    const request = yield axios.get(
      'https://rem-rest-api.herokuapp.com/api/users'
    );
    console.log(request);
    return request + 'correctly received';
  } catch (e) {
    console.log('Error in fetching records: ', e);
  }
}

const iterator = genToFetchUsers();
const iteration = iterator.next();
//Runs the gen upto first yield
//It is an api call and so the function returns a Promise, which is the  yielded and the execution is paused.
//iteration now holds unresolved Promise

//defines a fn to call when promise resolves(Helper function to resolve the promises)
iteration.value.then((resolved) => {
  resolved.data.data; //at this point we get the resolved value
  const nextIteration = iterator.next(resolved.data.data);
  // call next() to resume the execution of gen function passing the resolved values to the generator function as arguments to next ()
  console.log(nextIteration);
});
