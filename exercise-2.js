//Implementing Generators as Iterables
//No use of Symbol.iterator, next(), not neccessary to save state
function* genIterator() {
  yield 'This';
  yield 'is';
  yield 'iterable';
}

for (const i of genIterator()) {
  console.log(i);
}
